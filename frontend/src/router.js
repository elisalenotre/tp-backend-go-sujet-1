import { createRouter, createWebHistory } from 'vue-router';

const routes = [
    {
        path: '/:catchAll(.*)',
        redirect: '/form-connexion',
    },
    // Api
    {
        path: '/api-data',
        name: 'ApiData',
        component: () => import('@/views/ApiData.vue'),
    },
    // Clients
    {
        path: '/salons-list',
        name: 'SalonsList',
        component: () => import('@/views/clients/SalonsList.vue'),
    },
    {
        path: '/coiffeurs-list/:id_salon',
        name: 'CoiffeursList',
        component: () => import('@/views/clients/CoiffeursList.vue'),
    },
    {
        path: '/dispo-coiffeur/:id_coiffeur',
        name: 'DispoCoiffeur',
        component: () => import('@/views/clients/DispoCoiffeur.vue'),
    },
    {
        path: '/mes-rdv',
        name: 'MesRdv',
        component: () => import('@/views/clients/MesRdv.vue'),
    },
    // Pros
    {
        path: '/create-salon',
        name: 'CreateSalon',
        component: () => import('@/views/pros/CreateSalon.vue'),
    },
    {
        path: '/create-coiffeur',
        name: 'CreateCoiffeur',
        component: () => import('@/views/pros/CreateCoiffeur.vue'),
    },
    {
        path: '/create-disponibilite/:id_coiffeur',
        name: 'CreateDisponibilite',
        component: () => import('@/views/pros/CreateDisponibilite.vue'),
    },
    // Admin
    {
        path: '/back-office-clients',
        name: 'BackOfficeClients',
        component: () => import('@/views/admins/BackOfficeClients.vue'),
    },
    {
        path: '/back-office-salons',
        name: 'BackOfficeSalons',
        component: () => import('@/views/admins/BackOfficeSalons.vue'),
    },
    {
        path: '/back-office-coiffeurs',
        name: 'BackOfficeCoiffeurs',
        component: () => import('@/views/admins/BackOfficeCoiffeurs.vue'),
    },
    // Form inscription et connexion
    {
        path: '/form-inscription',
        name: 'FormInscri',
        component: () => import('@/views/FormInscri.vue'),
    },
    {
        path: '/form-connexion',
        name: 'FormConnex',
        component: () => import('@/views/FormConnex.vue'),
    },
];

const router = createRouter({
    history: createWebHistory(),  
    routes,
});

export default router;
