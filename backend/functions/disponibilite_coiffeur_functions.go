// disponibilite_coiffeur_routes.go
package functions

import (
	"encoding/json"
	"net/http"
	"strconv"
	"backend/database"
)

type Disponibilite struct {
	ID           int    `json:"id_disponibilite"`
	CoiffeurID   int    `json:"coiffeur_id"`
	JourSemaine  string `json:"jour_semaine"`
	Heure        string `json:"heure"`
}

func GetDisponibilitesCoiffeurs(w http.ResponseWriter, r *http.Request) {
	var disponibilites []Disponibilite
	rows, err := database.Db.Query("SELECT disponibilite_id, coiffeur_id, jour_semaine, heure FROM disponibilites_coiffeurs")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	defer rows.Close()

	for rows.Next() {
		var disponibilite Disponibilite
		err := rows.Scan(&disponibilite.ID, &disponibilite.CoiffeurID, &disponibilite.JourSemaine, &disponibilite.Heure)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
		disponibilites = append(disponibilites, disponibilite)
	}

	response, err := json.Marshal(disponibilites)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

func GetDisponibiliteCoiffeur(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[len("/disponibilite-coiffeur/"):]
	disponibiliteID, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid disponibilite ID"))
		return
	}

	var disponibilite Disponibilite
	err = database.Db.QueryRow("SELECT disponibilite_id, coiffeur_id, jour_semaine, heure FROM disponibilites_coiffeurs WHERE disponibilite_id = ?", disponibiliteID).
		Scan(&disponibilite.ID, &disponibilite.CoiffeurID, &disponibilite.JourSemaine, &disponibilite.Heure)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Disponibilite not found"))
		return
	}

	response, err := json.Marshal(disponibilite)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

func CreateDisponibiliteCoiffeur(w http.ResponseWriter, r *http.Request) {
	var disponibilite Disponibilite
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&disponibilite)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	defer r.Body.Close()

	result, err := database.Db.Exec("INSERT INTO disponibilites_coiffeurs (coiffeur_id, jour_semaine, heure) VALUES (?, ?, ?)",
		disponibilite.CoiffeurID, disponibilite.JourSemaine, disponibilite.Heure)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	disponibiliteID, _ := result.LastInsertId()
	disponibilite.ID = int(disponibiliteID)

	response, err := json.Marshal(disponibilite)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(response)
}

func UpdateDisponibiliteCoiffeur(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[len("/update-disponibilite-coiffeur/"):]
	disponibiliteID, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid disponibilite ID"))
		return
	}

	var disponibilite Disponibilite
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&disponibilite)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	defer r.Body.Close()

	_, err = database.Db.Exec("UPDATE disponibilites_coiffeurs SET coiffeur_id = ?, jour_semaine = ?, heure = ? WHERE disponibilite_id = ?",
		disponibilite.CoiffeurID, disponibilite.JourSemaine, disponibilite.Heure, disponibiliteID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"message": "disponibilite updated successfully"})
}

func DeleteDisponibiliteCoiffeur(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[len("/delete-disponibilite-coiffeur/"):]
	disponibiliteID, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid disponibilite ID"))
		return
	}

	_, err = database.Db.Exec("DELETE FROM disponibilites_coiffeurs WHERE disponibilite_id = ?", disponibiliteID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"message": "disponibilite deleted successfully"})
}
