// salon_functions.go
package functions

import (
	"encoding/json"
	"net/http"
	"strconv"
	"backend/database"
)

type Salon struct {
	ID             int    `json:"id_salon"`
	NomSalon       string `json:"nom_salon"`
	Emplacement    string `json:"emplacement"`
	ProprietaireID int    `json:"proprietaire_id"`
}

func GetSalons(w http.ResponseWriter, r *http.Request) {
	var salons []Salon
	rows, err := database.Db.Query("SELECT id_salon, nom_salon, emplacement, proprietaire_id FROM salons")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	defer rows.Close()

	for rows.Next() {
		var salon Salon
		err := rows.Scan(&salon.ID, &salon.NomSalon, &salon.Emplacement, &salon.ProprietaireID)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
		salons = append(salons, salon)
	}

	response, err := json.Marshal(salons)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

func GetSalon(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[len("/salon/"):]
	salonID, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid salon ID"))
		return
	}

	var salon Salon
	err = database.Db.QueryRow("SELECT id_salon, nom_salon, emplacement, proprietaire_id FROM salons WHERE id_salon = ?", salonID).
		Scan(&salon.ID, &salon.NomSalon, &salon.Emplacement, &salon.ProprietaireID)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Salon not found"))
		return
	}

	response, err := json.Marshal(salon)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

func CreateSalon(w http.ResponseWriter, r *http.Request) {
	var salon Salon
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&salon)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	defer r.Body.Close()

	result, err := database.Db.Exec("INSERT INTO salons (nom_salon, emplacement, proprietaire_id) VALUES (?, ?, ?)",
		salon.NomSalon, salon.Emplacement, salon.ProprietaireID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	salonID, _ := result.LastInsertId()
	salon.ID = int(salonID)

	response, err := json.Marshal(salon)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(response)
}

func UpdateSalon(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[len("/update-salon/"):]
	salonID, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid salon ID"))
		return
	}

	var salon Salon
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&salon)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	defer r.Body.Close()

	_, err = database.Db.Exec("UPDATE salons SET nom_salon = ?, emplacement = ?, proprietaire_id = ? WHERE id_salon = ?",
		salon.NomSalon, salon.Emplacement, salon.ProprietaireID, salonID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"message": "salon updated successfully"})
}

func DeleteSalon(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[len("/delete-salon/"):]
	salonID, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid salon ID"))
		return
	}

	_, err = database.Db.Exec("DELETE FROM salons WHERE id_salon = ?", salonID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"message": "salon deleted successfully"})
}
