// user_functions.go
package functions

import (
	"encoding/json"
	"net/http"
	"strconv"
	"backend/database"
)

type User struct {
	ID         int    `json:"id_user"`
	Nom        string `json:"nom_user"`
	Email      string `json:"email_user"`
	MotDePasse string `json:"mot_de_passe"`
	TypeUser   string `json:"type_user"`
}

func GetUsers(w http.ResponseWriter, r *http.Request) {
	var users []User
	rows, err := database.Db.Query("SELECT id_user, nom_user, email_user, mot_de_passe, type_user FROM users")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	defer rows.Close()

	for rows.Next() {
		var user User
		err := rows.Scan(&user.ID, &user.Nom, &user.Email, &user.MotDePasse, &user.TypeUser)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
		users = append(users, user)
	}

	response, err := json.Marshal(users)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

func GetUser(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[len("/user/"):]
	userID, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid user ID"))
		return
	}

	var user User
	err = database.Db.QueryRow("SELECT id_user, nom_user, email_user, mot_de_passe, type_user FROM users WHERE id_user = ?", userID).
		Scan(&user.ID, &user.Nom, &user.Email, &user.MotDePasse, &user.TypeUser)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("User not found"))
		return
	}

	response, err := json.Marshal(user)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

func CreateUser(w http.ResponseWriter, r *http.Request) {
	var user User
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	defer r.Body.Close()

	result, err := database.Db.Exec("INSERT INTO users (nom_user, email_user, mot_de_passe, type_user) VALUES (?, ?, ?, ?)",
		user.Nom, user.Email, user.MotDePasse, user.TypeUser)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	userID, _ := result.LastInsertId()
	user.ID = int(userID)

	response, err := json.Marshal(user)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(response)
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[len("/update-user/"):]
	userID, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid user ID"))
		return
	}

	var user User
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	defer r.Body.Close()

	_, err = database.Db.Exec("UPDATE users SET nom_user = ?, email_user = ?, mot_de_passe = ?, type_user = ? WHERE id_user = ?",
		user.Nom, user.Email, user.MotDePasse, user.TypeUser, userID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"message": "user updated successfully"})
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
    id := r.URL.Path[len("/delete-user/"):]
    userID, err := strconv.Atoi(id)
    if err != nil {
        w.WriteHeader(http.StatusBadRequest)
        w.Write([]byte("Invalid user ID"))
        return
    }

    _, err = database.Db.Exec("DELETE FROM users WHERE id_user = ?", userID)
    if err != nil {
        w.WriteHeader(http.StatusInternalServerError)
        w.Write([]byte(err.Error()))
        return
    }

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"message": "user deleted successfully"})
}
