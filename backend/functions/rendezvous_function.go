// functions/rendezvous_functions.go
package functions

import (
	"encoding/json"
	"net/http"
	"strconv"
	"backend/database"
)

type RendezVousUtilisateur struct {
	ID            int    `json:"id_rendezvous"`
	UtilisateurID int    `json:"utilisateur_id"`
	CoiffeurID    int    `json:"coiffeur_id"`
	Date          string `json:"date"`
}

func GetRendezVousUtilisateurs(w http.ResponseWriter, r *http.Request) {
	var rendezvous []RendezVousUtilisateur
	rows, err := database.Db.Query("SELECT rdv_utilisateur_id, utilisateur_id, coiffeur_id, date FROM rendezvous_utilisateurs")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	defer rows.Close()

	for rows.Next() {
		var rdv RendezVousUtilisateur
		err := rows.Scan(&rdv.ID, &rdv.UtilisateurID, &rdv.CoiffeurID, &rdv.Date)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
		rendezvous = append(rendezvous, rdv)
	}

	response, err := json.Marshal(rendezvous)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

func GetRendezVousUtilisateur(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[len("/rendezvous/"):]
	rdvID, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid rendezvous ID"))
		return
	}

	var rdv RendezVousUtilisateur
	err = database.Db.QueryRow("SELECT rdv_utilisateur_id, utilisateur_id, coiffeur_id, date FROM rendezvous_utilisateurs WHERE rdv_utilisateur_id = ?", rdvID).
		Scan(&rdv.ID, &rdv.UtilisateurID, &rdv.CoiffeurID, &rdv.Date)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Rendezvous not found"))
		return
	}

	response, err := json.Marshal(rdv)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

func CreateRendezVousUtilisateur(w http.ResponseWriter, r *http.Request) {
	var rdv RendezVousUtilisateur
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&rdv)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	defer r.Body.Close()

	result, err := database.Db.Exec("INSERT INTO rendezvous_utilisateurs (utilisateur_id, coiffeur_id, date) VALUES (?, ?, ?)",
		rdv.UtilisateurID, rdv.CoiffeurID, rdv.Date)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	rdvID, _ := result.LastInsertId()
	rdv.ID = int(rdvID)

	response, err := json.Marshal(rdv)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(response)
}

func UpdateRendezVousUtilisateur(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[len("/update-rendezvous/"):]
	rdvID, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid rendezvous ID"))
		return
	}

	var rdv RendezVousUtilisateur
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&rdv)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	defer r.Body.Close()

	_, err = database.Db.Exec("UPDATE rendezvous_utilisateurs SET utilisateur_id = ?, coiffeur_id = ?, date = ? WHERE rdv_utilisateur_id = ?",
		rdv.UtilisateurID, rdv.CoiffeurID, rdv.Date, rdvID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"message": "rendezvous updated successfully"})
}

func DeleteRendezVousUtilisateur(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[len("/delete-rendezvous/"):]
	rdvID, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid rendezvous ID"))
		return
	}

	_, err = database.Db.Exec("DELETE FROM rendezvous_utilisateurs WHERE rdv_utilisateur_id = ?", rdvID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"message": "rendezvous deleted successfully"})
}
