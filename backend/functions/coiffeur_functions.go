// coiffeur_functions.go
package functions

import (
	"encoding/json"
	"net/http"
	"strconv"
	"backend/database"
)

type Coiffeur struct {
	ID         int    `json:"id_coiffeur"`
	NomCoiffeur string `json:"nom_coiffeur"`
	SalonID    int    `json:"salon_id"`
}

func GetCoiffeurs(w http.ResponseWriter, r *http.Request) {
	var coiffeurs []Coiffeur
	rows, err := database.Db.Query("SELECT coiffeur_id, nom_coiffeur, salon_id FROM coiffeurs")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	defer rows.Close()

	for rows.Next() {
		var coiffeur Coiffeur
		err := rows.Scan(&coiffeur.ID, &coiffeur.NomCoiffeur, &coiffeur.SalonID)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
		coiffeurs = append(coiffeurs, coiffeur)
	}

	response, err := json.Marshal(coiffeurs)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

func GetCoiffeur(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[len("/coiffeur/"):]
	coiffeurID, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid coiffeur ID"))
		return
	}

	var coiffeur Coiffeur
	err = database.Db.QueryRow("SELECT coiffeur_id, nom_coiffeur, salon_id FROM coiffeurs WHERE coiffeur_id = ?", coiffeurID).
		Scan(&coiffeur.ID, &coiffeur.NomCoiffeur, &coiffeur.SalonID)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Coiffeur not found"))
		return
	}

	response, err := json.Marshal(coiffeur)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

func CreateCoiffeur(w http.ResponseWriter, r *http.Request) {
	var coiffeur Coiffeur
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&coiffeur)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	defer r.Body.Close()

	result, err := database.Db.Exec("INSERT INTO coiffeurs (nom_coiffeur, salon_id) VALUES (?, ?)",
		coiffeur.NomCoiffeur, coiffeur.SalonID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	coiffeurID, _ := result.LastInsertId()
	coiffeur.ID = int(coiffeurID)

	response, err := json.Marshal(coiffeur)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(response)
}

func UpdateCoiffeur(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[len("/update-coiffeur/"):]
	coiffeurID, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid coiffeur ID"))
		return
	}

	var coiffeur Coiffeur
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&coiffeur)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	defer r.Body.Close()

	_, err = database.Db.Exec("UPDATE coiffeurs SET nom_coiffeur = ?, salon_id = ? WHERE coiffeur_id = ?",
		coiffeur.NomCoiffeur, coiffeur.SalonID, coiffeurID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"message": "Coiffeur updated successfully"})
}

func DeleteCoiffeur(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[len("/delete-coiffeur/"):]
	coiffeurID, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid coiffeur ID"))
		return
	}

	_, err = database.Db.Exec("DELETE FROM coiffeurs WHERE coiffeur_id = ?", coiffeurID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"message": "Coiffeur deleted successfully"})
}
