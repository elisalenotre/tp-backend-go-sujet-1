// demande_compte_pro_functions.go
package functions

import (
	"encoding/json"
	"net/http"
	"strconv"
	"backend/database"
)

type DemandeComptePro struct {
	ID            int    `json:"id_demande"`
	UtilisateurID int    `json:"utilisateur_id"`
	Statut        string `json:"statut"`
}

func GetDemandesComptePro(w http.ResponseWriter, r *http.Request) {
	var demandes []DemandeComptePro
	rows, err := database.Db.Query("SELECT demande_id, utilisateur_id, statut FROM demandes_compte_pro")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	defer rows.Close()

	for rows.Next() {
		var demande DemandeComptePro
		err := rows.Scan(&demande.ID, &demande.UtilisateurID, &demande.Statut)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
		demandes = append(demandes, demande)
	}

	response, err := json.Marshal(demandes)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

func GetDemandeComptePro(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[len("/demande-compte-pro/"):]
	demandeID, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid demande ID"))
		return
	}

	var demande DemandeComptePro
	err = database.Db.QueryRow("SELECT demande_id, utilisateur_id, statut FROM demandes_compte_pro WHERE demande_id = ?", demandeID).
		Scan(&demande.ID, &demande.UtilisateurID, &demande.Statut)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Demande not found"))
		return
	}

	response, err := json.Marshal(demande)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

func CreateDemandeComptePro(w http.ResponseWriter, r *http.Request) {
	var demande DemandeComptePro
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&demande)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	defer r.Body.Close()

	result, err := database.Db.Exec("INSERT INTO demandes_compte_pro (utilisateur_id, statut) VALUES (?, ?)",
		demande.UtilisateurID, demande.Statut)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	demandeID, _ := result.LastInsertId()
	demande.ID = int(demandeID)

	response, err := json.Marshal(demande)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(response)
}

func UpdateDemandeComptePro(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[len("/update-demande-compte-pro/"):]
	demandeID, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid demande ID"))
		return
	}

	var demande DemandeComptePro
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&demande)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	defer r.Body.Close()

	_, err = database.Db.Exec("UPDATE demandes_compte_pro SET utilisateur_id = ?, statut = ? WHERE demande_id = ?",
		demande.UtilisateurID, demande.Statut, demandeID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"message": "demande updated successfully"})
}

func DeleteDemandeComptePro(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[len("/delete-demande-compte-pro/"):]
	demandeID, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid demande ID"))
		return
	}

	_, err = database.Db.Exec("DELETE FROM demandes_compte_pro WHERE demande_id = ?", demandeID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"message": "demande deleted successfully"})
}
