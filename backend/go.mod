module backend

go 1.21.6

require (
	github.com/go-gomail/gomail v0.0.0-20160411212932-81ebce5c23df // indirect
	github.com/mattn/go-sqlite3 v1.14.20 // indirect
	github.com/rs/cors v1.10.1 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
)
