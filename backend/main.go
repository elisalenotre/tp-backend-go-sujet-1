package main

import (
	"backend/database"
	"backend/routes"
	"log"
	"net/http"
)

func main() {
	err := database.InitDB()
	if err != nil {
		log.Fatal(err)
	}

	mux := http.NewServeMux()
	routes.SetupRoutes(mux)

	handler := corsHandler(mux)

	log.Fatal(http.ListenAndServe(":8000", handler))
}

func corsHandler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization")

		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusOK)
			return
		}

		h.ServeHTTP(w, r)
	})
}
