// coiffeur_routes.go
package routes

import (
	"net/http"
	"backend/functions"
)

func SetupCoiffeurRoutes(mux *http.ServeMux) {
	mux.HandleFunc("/coiffeurs", functions.GetCoiffeurs)
	mux.HandleFunc("/coiffeur/", functions.GetCoiffeur)
	mux.HandleFunc("/create-coiffeur", functions.CreateCoiffeur)
	mux.HandleFunc("/update-coiffeur/", functions.UpdateCoiffeur)
	mux.HandleFunc("/delete-coiffeur/", functions.DeleteCoiffeur)
}
