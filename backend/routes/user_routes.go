// user_routes.go
package routes

import (
	"net/http"
	"backend/functions"
)

func SetupUserRoutes(mux *http.ServeMux) {
	mux.HandleFunc("/users", functions.GetUsers)
	mux.HandleFunc("/user/", functions.GetUser)
	mux.HandleFunc("/create-user", functions.CreateUser)  
	mux.HandleFunc("/update-user/", functions.UpdateUser)  
	mux.HandleFunc("/delete-user/", functions.DeleteUser)  
}
