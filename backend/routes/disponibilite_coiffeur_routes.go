// disponibilite_coiffeur_routes.go
package routes

import (
	"net/http"
	"backend/functions"
)

func SetupDisponibiliteCoiffeurRoutes(mux *http.ServeMux) {
	mux.HandleFunc("/disponibilites-coiffeurs", functions.GetDisponibilitesCoiffeurs)
	mux.HandleFunc("/disponibilite-coiffeur/", functions.GetDisponibiliteCoiffeur)
	mux.HandleFunc("/create-disponibilite-coiffeur", functions.CreateDisponibiliteCoiffeur)
	mux.HandleFunc("/update-disponibilite-coiffeur/", functions.UpdateDisponibiliteCoiffeur)
	mux.HandleFunc("/delete-disponibilite-coiffeur/", functions.DeleteDisponibiliteCoiffeur)
}
