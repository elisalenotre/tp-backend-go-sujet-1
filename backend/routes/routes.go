// routes.go
package routes

import (
	"net/http"
	"backend/database"
)

func SetupRoutes(mux *http.ServeMux) {
	mux.HandleFunc("/health", HealthCheck)
	SetupUserRoutes(mux)
	SetupSalonRoutes(mux)
	SetupCoiffeurRoutes(mux)
	SetupDisponibiliteCoiffeurRoutes(mux)
	SetupDemandeCompteProRoutes(mux)
	SetupRendezVousUtilisateursRoutes(mux)
}

func HealthCheck(w http.ResponseWriter, r *http.Request) {
	err := database.Db.Ping()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Impossible de se connecter à la base de données"))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Connecté à la base de données"))
}
