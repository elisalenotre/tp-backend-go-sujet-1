// salon_routes.go
package routes

import (
	"net/http"
	"backend/functions"
)

func SetupSalonRoutes(mux *http.ServeMux) {
	mux.HandleFunc("/salons", functions.GetSalons)
	mux.HandleFunc("/salon/", functions.GetSalon)
	mux.HandleFunc("/create-salon", functions.CreateSalon)  
	mux.HandleFunc("/update-salon/", functions.UpdateSalon)  
	mux.HandleFunc("/delete-salon/", functions.DeleteSalon) 
}