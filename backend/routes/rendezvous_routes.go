// rendezvous_routes.go
package routes

import (
	"net/http"
	"backend/functions"
)

func SetupRendezVousUtilisateursRoutes(mux *http.ServeMux) {
	mux.HandleFunc("/rendezvous", functions.GetRendezVousUtilisateurs)
	mux.HandleFunc("/rendezvous/", functions.GetRendezVousUtilisateur)
	mux.HandleFunc("/create-rendezvous", functions.CreateRendezVousUtilisateur)
	mux.HandleFunc("/update-rendezvous/", functions.UpdateRendezVousUtilisateur)
	mux.HandleFunc("/delete-rendezvous/", functions.DeleteRendezVousUtilisateur)
}