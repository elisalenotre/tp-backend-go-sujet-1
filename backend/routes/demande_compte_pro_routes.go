// demande_compte_pro_routes.go
package routes

import (
	"net/http"
	"backend/functions"
)

func SetupDemandeCompteProRoutes(mux *http.ServeMux) {
	mux.HandleFunc("/demandes-compte-pro", functions.GetDemandesComptePro)
	mux.HandleFunc("/demande-compte-pro/", functions.GetDemandeComptePro)
	mux.HandleFunc("/create-demande-compte-pro", functions.CreateDemandeComptePro)
	mux.HandleFunc("/update-demande-compte-pro/", functions.UpdateDemandeComptePro)
	mux.HandleFunc("/delete-demande-compte-pro/", functions.DeleteDemandeComptePro)
}
