// salon_data.go
package database

import (
    "log"
)

func CreateSalonsTable() error {
    _, err := Db.Exec(`CREATE TABLE IF NOT EXISTS salons (
        id_salon INTEGER PRIMARY KEY AUTOINCREMENT,
        nom_salon TEXT,
        emplacement TEXT,
        proprietaire_id INTEGER,
        FOREIGN KEY (proprietaire_id) REFERENCES users (id_user)
    );`)
    if err != nil {
        log.Fatal(err)
        return err
    }

    return nil
}

func InsertSalons() error {
    _, err := Db.Exec(`
        INSERT INTO salons (nom_salon, emplacement, proprietaire_id)
        VALUES (?, ?, ?), (?, ?, ?), (?, ?, ?)`,
        "Salon1", "Emplacement1", 1,
        "Salon2", "Emplacement2", 2,
        "Salon3", "Emplacement3", 3)
    if err != nil {
        log.Fatal(err)
        return err
    }

    return nil
}
