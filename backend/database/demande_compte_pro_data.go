// demande_compte_pro_data.go
package database

import (
	"log"
)

// Fonction pour créer la table demandes_compte_pro
func CreateDemandesCompteProTable() error {
	_, err := Db.Exec(`CREATE TABLE IF NOT EXISTS demandes_compte_pro (
		demande_id INTEGER PRIMARY KEY AUTOINCREMENT,
		utilisateur_id INTEGER,
		statut TEXT,
		FOREIGN KEY (utilisateur_id) REFERENCES users (id_user)
	);`)
	if err != nil {
		log.Fatal(err)
		return err
	}

	return nil
}

// Fonction pour insérer des données initiales dans la table demandes_compte_pro
func InsertDemandesComptePro() error {
	_, err := Db.Exec(`
		INSERT INTO demandes_compte_pro (utilisateur_id, statut)
		VALUES (?, ?), (?, ?), (?, ?)`,
		1, "En attente",
		2, "Accepté",
		3, "Refusé")
	if err != nil {
		log.Fatal(err)
		return err
	}

	return nil
}
