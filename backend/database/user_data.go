// user_data.go
package database

import (
    "log"
)

func CreateUsersTable() error {
    _, err := Db.Exec(`CREATE TABLE IF NOT EXISTS users (
        id_user INTEGER PRIMARY KEY AUTOINCREMENT,
        nom_user TEXT,
        email_user TEXT,
        mot_de_passe TEXT,
        type_user TEXT
    );`)
    if err != nil {
        log.Fatal(err)
        return err
    }

    return nil
}

func InsertUsers() error {
    _, err := Db.Exec(`
        INSERT INTO users (nom_user, email_user, mot_de_passe, type_user)
        VALUES (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?)`,
        "client", "client@gmail.com", "client", "client",
        "pro", "pro@gmail.com", "pro", "professionnel",
        "admin", "admin@gmail.com", "admin", "admin")
    if err != nil {
        log.Fatal(err)
        return err
    }

    return nil
}
