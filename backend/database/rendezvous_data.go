// rendezvous_data.go
package database

import (
	"log"
)

// Function to create the rendezvous_utilisateurs table
func CreateRendezVousUtilisateursTable() error {
	_, err := Db.Exec(`CREATE TABLE IF NOT EXISTS rendezvous_utilisateurs (
		rdv_utilisateur_id INTEGER PRIMARY KEY AUTOINCREMENT,
		utilisateur_id INTEGER,
		coiffeur_id INTEGER,
		date TEXT,
		FOREIGN KEY (utilisateur_id) REFERENCES users (user_id),
		FOREIGN KEY (coiffeur_id) REFERENCES coiffeurs (coiffeur_id)
	);`)
	if err != nil {
		log.Fatal(err)
		return err
	}

	return nil
}

// Function to insert initial data into the rendezvous_utilisateurs table
func InsertRendezVousUtilisateurs() error {
	_, err := Db.Exec(`
		INSERT INTO rendezvous_utilisateurs (utilisateur_id, coiffeur_id, date)
		VALUES (?, ?, ?), (?, ?, ?), (?, ?, ?)`,
		1, 1, "2024-02-10 09:30",
		2, 2, "2024-02-15 14:00",
		3, 3, "2024-02-20 10:30")
	if err != nil {
		log.Fatal(err)
		return err
	}

	return nil
}
