// database.go
package database

import (
	"database/sql"
	"log"
	_ "github.com/mattn/go-sqlite3"
)

var Db *sql.DB

func InitDB() error {
	var err error

	Db, err = sql.Open("sqlite3", "./database.db")
	if err != nil {
		log.Fatal(err)
		return err
	}

	err = CreateUsersTable()
	if err != nil {
		return err
	}

	err = CreateSalonsTable()
	if err != nil {
		return err
	}

	err = CreateCoiffeursTable()
	if err != nil {
		return err
	}

	err = CreateDisponibilitesCoiffeursTable()
	if err != nil {
		return err
	}

	err = CreateDemandesCompteProTable()
	if err != nil {
		return err
	}

	err = CreateRendezVousUtilisateursTable()
	if err != nil {
		return err
	}

	err = InsertUsers()
	if err != nil {
		return err
	}

	err = InsertSalons()
	if err != nil {
		return err
	}

	err = InsertCoiffeurs()
	if err != nil {
		return err
	}

	err = InsertDisponibilitesCoiffeurs()
	if err != nil {
		return err
	}

	err = InsertDemandesComptePro()
	if err != nil {
		return err
	}

	err = InsertRendezVousUtilisateurs()
	if err != nil {
		return err
	}

	return nil
}
