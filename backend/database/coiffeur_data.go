// coiffeur_data.go
package database

import (
	"log"
)

func CreateCoiffeursTable() error {
	_, err := Db.Exec(`CREATE TABLE IF NOT EXISTS coiffeurs (
		coiffeur_id INTEGER PRIMARY KEY AUTOINCREMENT,
		nom_coiffeur TEXT,
		salon_id INTEGER,
		FOREIGN KEY (salon_id) REFERENCES salons (id_salon)
	);`)
	if err != nil {
		log.Fatal(err)
		return err
	}

	return nil
}

func InsertCoiffeurs() error {
	_, err := Db.Exec(`
		INSERT INTO coiffeurs (nom_coiffeur, salon_id)
		VALUES (?, ?), (?, ?), (?, ?)`,
		"Coiffeur1", 1,
		"Coiffeur2", 2,
		"Coiffeur3", 3)
	if err != nil {
		log.Fatal(err)
		return err
	}

	return nil
}
