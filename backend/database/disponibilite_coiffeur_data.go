// disponibilite_data.go
package database

import (
	"log"
)

// Function to create the disponibilites_coiffeurs table
func CreateDisponibilitesCoiffeursTable() error {
	_, err := Db.Exec(`CREATE TABLE IF NOT EXISTS disponibilites_coiffeurs (
		disponibilite_id INTEGER PRIMARY KEY AUTOINCREMENT,
		coiffeur_id INTEGER,
		jour_semaine TEXT,
		heure TEXT,
		FOREIGN KEY (coiffeur_id) REFERENCES coiffeurs (coiffeur_id)
	);`)
	if err != nil {
		log.Fatal(err)
		return err
	}

	return nil
}

// Function to insert initial data into the disponibilites_coiffeurs table
func InsertDisponibilitesCoiffeurs() error {
	_, err := Db.Exec(`
		INSERT INTO disponibilites_coiffeurs (coiffeur_id, jour_semaine, heure)
		VALUES (?, ?, ?), (?, ?, ?), (?, ?, ?)`,
		1, "Lundi", "09:00",
		2, "Mardi", "14:00",
		3, "Mercredi", "10:00")
	if err != nil {
		log.Fatal(err)
		return err
	}

	return nil
}
